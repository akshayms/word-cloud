# Scalable word cloud

## Installation
Requirements:

 + Java 8
 + Maven 3.3+
 + RethinkDB: Instructions to install on [Ubuntu](https://www.rethinkdb.com/docs/install/ubuntu/)/[OS X](https://www.rethinkdb.com/docs/install/osx/)

Run the following commands after starting RethinkDB

    mvn clean install -DskipTests assembly:single
    cd target/
    java -jar word-cloud-0.1-SNAPSHOT-jar-with-dependencies.jar <RethinkHost> <RethinkPort> <HazelcastNode>

To add another machine to the cluster, run the application and pass the IP of the first machine as the argument in place of `HazelcastNode`. RethinkDB has to be installed on only one machine which has to be accessible from all the other nodes in the cluster. Default RethinkPort is 28015.

Every node in the cluster can accept requests and can serve the results of the word cloud. The following are the parameters to pass to the `simulateRequests.sh`

    Server: IP of any node in the cluster
    Port: 8000
    Param: URL

To check the word counts for a word clout, hit the following URL:

    http://<IP>:8000/wordCloud/

IP can be any node in the cluster. By default, the top 25 words are displayed but this can be tuned by recompiling after changing the value of `WC_NUM_WORDS` in `util.Constants`.

## Design

The project uses the following libraries. A detailed discussion of how they are put together and the architecture is in the next section.

+ Jetty - Lightweight embedded HTTP Server
+ Lucene - Its used for tokenization but there are multiple implementations of the parser in the codebase (`IParser` implementations)
+ JSoup - Library to traverse the DOM and to extract product information
+ Hazelcast - Distributed in memory data grid
+ RethinkDB - Database for persistence

## Data Layer

The data layer has two main components: Hazelcast and RethinkDB. Hazelcast is a distributed in-memory datastore that offers distributed versions of java.util.Collections. It has options to partition, replicate, topical pub/sub, event based listers and many more options to fine tune application guarantees.

The URLs that we encounter has to be durable and the size of this set grows over time. Every node in the cluster needs to be aware of URLs already parsed and this has to be durable. RethinkDB is used as the persistence layer and Hazelcast acts as an intermediate cache. Every new URL is written through to the database before being reflected in the map and any URL that is not in the cache results in a disk read through the persistence layer, in case that URL is not present in the partition where the request goes.

The word counts are not written out to disk as soon as they are encountered. It is present in memory and is eventually written out to disk. This is configurable but the default write delay is set to 5 seconds. By avoiding the cost of an IO for every word count increment, the application can lower the response time for those URLs that are unseen. Hazelcast resolves conflict by applying the last write, but the documentation is unclear about how the last write is determined. There is an optimization (commented out in the code) in place where any URL encountered is passed to a thread pool and the client will get a response immediately allowing for more throughput, if that's required. 

The level of consistency and resilience for the word counts can be fine tuned by explicitly defining backup, partitioning and merge behavior depending on application needs.

Atomic updates to the word count is done by sending the code to the data instead of moving the data to the code. The code is sent to the partition owner of the key and Hazelcast will ensure exclusive write access for the duration of the update by acquiring necessary locks internally.

### Failure Scenarios

There are at least two known failure scenarios that are not covered in the code but the design allows for defining the behavior to recover from these failures. The known failure scenarios that are uncovered are -

  1. After writing through to RethinkDB, the node crashes immediately and the word counts are never reflected and subsequent calls to that include that URL will be rejected because its marked as seen.
  2. In case of a split-brain scenario where the cluster is split into two groups, the merge policy is undefined at the moment. Hazelcast has some merge strategies that depend on the time of the update but the documentation is unclear about how the time is synchronized.

The URL store is a Map instead of a Set. The design is to allow for extensions to track the URL parsing progress (`STARTING, PARSED, FLUSHED_TO_DISK`). However, this doesn't help in solving partial rollback in case of failures midway but should be helpful in quantifying the inconsistency in the word counts.

RethinkDB is a single point of failure. It supports a clustered mode which can remove the SPOF but the operational complexity will be much higher and the consistency models of a clustered setup needs to be understood better before scaling out Rethink as-is in the current setup.

The application is designed for low-latency updates to the word counts and durable, consistent updates to the parsed URLs. The application is flexible to change the guarantees offered to make it consistent or available with few modifications to the code.

The application currently has no strong dependencies on the underlying database either and can be switched out by reimplementing the two persistence package classes to push updates to the underlying database.

### Other Ideas
Another key insight that could potentially improve the scalability of the application by eliminating the need for any locking is by using CRDTs. The counter is an up-counter and increment operations commute, that is, the updates can be applied out-of-order and we can still infer the final values by merging CRDTs that are partitioned by the key space. There are subtle gotchas but data stores such as Riak offer CRDT implementations. However, this idea was conceived too late and durability with Riak can still pose certain persistence challenges. The operational complexity of Riak seemed slightly higher than with Rethink + Hazelcast to make the switch at a late stage of development but is definitely worth considering. We could technically use this approach with Hazelcast + Rethink but is slightly more complicated than just using a counter out of the box in Riak.