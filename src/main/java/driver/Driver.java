package driver;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import rest.AmazonProductRequestHandler;
import rest.WordCloudRequestHandler;
import services.WordCloudUpdater;
import util.ClusterConfig;

/**
 * Created by amanchalesridhar on 8/5/16.
 */
public class Driver {

    private static final int PORT = 8000;

    private static WordCloudUpdater wordCloudUpdater;

    public static void main(String[] args) {

        if (args.length < 3) {
            System.out.println("Please provide the following 3 arguments: DBHost, DBport, HazelcastIP");
            System.exit(1);
        }

        String dbHost = args[0];
        int dbPort = Integer.parseInt(args[1]);
        String hazelcastIp = args[2];

//        System.out.println("dbHost : " + dbHost + " port : " + dbPort + " hz " + hazelcastIp);
        ClusterConfig.setDbHostName(dbHost);
        ClusterConfig.setDbPort(dbPort);
        ClusterConfig.setHazelcastNode(hazelcastIp);

        wordCloudUpdater = new WordCloudUpdater(ClusterConfig.getHazelcastNode());
        startJettyServer(PORT);
    }

    private static void startJettyServer(int port) {
        Server server = new Server(port);

        ContextHandler postContext = new ContextHandler();
        postContext.setContextPath("/");
        postContext.setResourceBase(".");
        postContext.setClassLoader(Thread.currentThread().getContextClassLoader());
        postContext.setHandler(new AmazonProductRequestHandler(wordCloudUpdater));

        ContextHandler wcContext = new ContextHandler();
        wcContext.setContextPath("/wordCloud");
        wcContext.setResourceBase(".");
        wcContext.setClassLoader(Thread.currentThread().getContextClassLoader());
        wcContext.setHandler(new WordCloudRequestHandler(wordCloudUpdater));

        ContextHandlerCollection contexts = new ContextHandlerCollection();
        contexts.setHandlers(new Handler[] {postContext, wcContext});
        server.setHandler(contexts);

        try {
            server.start();
            server.join();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
