package persistence;

import com.hazelcast.core.MapStore;
import com.rethinkdb.RethinkDB;
import com.rethinkdb.net.Connection;
import com.rethinkdb.net.Cursor;
import util.ClusterConfig;
import util.Constants;

import java.util.*;

/**
 * Created by msa on 8/9/16.
 */
public class WordCountStore implements MapStore<String, Long> {

    private RethinkDB r;

    private Connection conn;

    public WordCountStore() {
        this.r = RethinkDB.r;
        this.conn = r.connection()
                .hostname(ClusterConfig.getDbHostName())
                .port(ClusterConfig.getDbPort())
                .connect();
    }

    @Override
    public void store(String s, Long aLong) {
        r.table(Constants.WC_TABLE)
                .insert(r.hashMap("word", s).with("count", aLong))
                .optArg("conflict", "replace")
                .run(conn);
    }

    @Override
    public void storeAll(Map<String, Long> map) {
        map.entrySet()
                .stream()
                .forEach(x -> store(x.getKey(), x.getValue()));
    }

    @Override
    public void delete(String s) {
        // Not required
    }

    @Override
    public void deleteAll(Collection<String> collection) {
        // Not required
    }

    @Override
    public Long load(String s) {
        Cursor cursor = r.table(Constants.WC_TABLE)
                .filter(r.hashMap("word", s))
                .run(conn);
        if (cursor.hasNext()) {
            return ((HashMap<String, Long>) cursor.next()).get("count");
        }
        else {
            return null;
        }
    }

    @Override
    public Map<String, Long> loadAll(Collection<String> collection) {
        Map<String, Long> allValues = new HashMap<String, Long>();
        collection.stream()
                .forEach(key -> allValues.put(key, load(key)));
        return allValues;
    }

    @Override
    public Iterable<String> loadAllKeys() {
        Cursor cursor = r.table(Constants.WC_TABLE).run(conn);
        Set<String> keys = new HashSet<>();
        for (Object object : cursor) {
            keys.add(((HashMap<String, String>)object).get("word"));
        }
        return keys;
    }
}
