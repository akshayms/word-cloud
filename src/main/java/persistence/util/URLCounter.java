package persistence.util;

import com.hazelcast.map.EntryBackupProcessor;
import com.hazelcast.map.EntryProcessor;

import java.util.Map;

/**
 * Created by msa on 8/11/16.
 */
public class URLCounter implements EntryProcessor<String, Long> {
    @Override
    public Object process(Map.Entry<String, Long> entry) {
        if (entry.getValue() == null) {
            entry.setValue(1L);
            return true;
        }
        return false;
    }

    @Override
    public EntryBackupProcessor<String, Long> getBackupProcessor() {
        return null;
    }
}
