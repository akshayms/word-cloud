package persistence.util;

import com.hazelcast.map.EntryBackupProcessor;
import com.hazelcast.map.EntryProcessor;

import java.util.Map;

/**
 * Created by msa on 8/11/16.
 */
public class WordCounter implements EntryProcessor<String, Long> {
    @Override
    public Object process(Map.Entry<String, Long> entry) {
        Long val = entry.getValue();
        if (val == null) {
            val = 1L;
        } else {
            val++;
        }
        entry.setValue(val);
        return val;
    }

    @Override
    public EntryBackupProcessor<String, Long> getBackupProcessor() {
        return null;
    }
}
