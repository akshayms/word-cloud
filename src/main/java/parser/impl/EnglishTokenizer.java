package parser.impl;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import parser.ITokenizer;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by amanchalesridhar on 8/5/16.
 */
public class EnglishTokenizer implements ITokenizer {
    @Override
    public List<String> tokenize(String text) {
        StandardAnalyzer analyzer = new StandardAnalyzer();
        TokenStream stream = analyzer.tokenStream(null, new StringReader(text));
        List<String> tokens = new ArrayList<>();
        try {
            stream.reset();
            while (stream.incrementToken()) {
                tokens.add(stream.getAttribute(CharTermAttribute.class).toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return tokens;
    }
}
