package parser.impl;

import parser.ITokenizer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by amanchalesridhar on 8/5/16.
 */
public class CustomTokenizer implements ITokenizer {

    private static Set<String> stopWords;

    static {
//        stopWords = new HashSet<>();
        try {
            InputStream stream = CustomTokenizer.class.getResourceAsStream("/stopWords.txt");
            stopWords = new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8))
                    .lines()
                    .map(term -> term.trim())
                    .collect(Collectors.toSet());
        } catch (Exception e) {
            System.out.println("Unable to load stop words!");
            System.exit(-1);
            e.printStackTrace();
        }
    }

    @Override
    public List<String> tokenize(String text) {
        return Arrays.stream(text.split(" "))
                .map(term -> cleanUp(term))
                .filter(term -> term.length() > 0)
                .map(term -> stemmer(term))
                .filter(term -> stop(term))
                //.filter(term -> isValidWord(term))
                .collect(Collectors.toList());
    }

    private String cleanUp(String term) {
        return term.trim().toLowerCase().replaceAll("[^a-zA-Z]+","");
    }

    private String stemmer(String term) {
        //TODO: Implement a stemmer
        return term;
    }

    private boolean stop(String term) {
        return !stopWords.contains(term);
    }
}
