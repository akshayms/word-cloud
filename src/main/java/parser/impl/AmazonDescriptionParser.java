package parser.impl;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import parser.IParser;

import java.io.IOException;

/**
 * Created by amanchalesridhar on 8/5/16.
 */
public class AmazonDescriptionParser implements IParser {

    public String getContent(String url) {
        try {
            //Document doc = Jsoup.parse(StringUtils.join(Files.readAllLines(Paths.get("/Users/amanchalesridhar/temp.html")), "\n"));
            //String html = Jsoup.connect(url).userAgent("Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_3_3 like Mac OS X; " +
              //      "en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8J2 Safari/6533.18.5")
                //    .timeout(390000).get().html();
            //header("User-agent", "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36")
            //System.out.println(html);
            String html = Jsoup.connect(url).userAgent("Mozilla/5.0 (Android; Mobile; rv:40.0) Gecko/40.0 Firefox/40" +
                    ".0").timeout(10000).get().html();
            Document doc = Jsoup.parse(html);
            return doc.getElementById("productDescription_secondary_view_div").text();
        } catch (IOException | NullPointerException e) {
            System.out.println("Could not load content from URL, aborting request: " + url);
            e.printStackTrace();
        }
        return null;
    }

}
