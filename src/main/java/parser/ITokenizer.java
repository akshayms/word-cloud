package parser;

import java.util.List;

/**
 * Created by amanchalesridhar on 8/5/16.
 */
public interface ITokenizer {

    List<String> tokenize(String text);

}
