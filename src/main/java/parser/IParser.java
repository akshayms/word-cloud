package parser;

/**
 * Created by amanchalesridhar on 8/5/16.
 */
public interface IParser {
    String getContent(String URL);
}
