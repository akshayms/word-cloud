package services;

import org.apache.commons.lang3.tuple.Pair;
import parser.IParser;
import parser.ITokenizer;
import parser.impl.AmazonDescriptionParser;
import parser.impl.CustomTokenizer;



import java.util.List;

/**
 * Created by amanchalesridhar on 8/5/16.
 */
public class WordCloudUpdater {

    private IParser parser;

    private ITokenizer tokenizer;

    private HazelcastStorageController dataStore;

    //private Map<String, Integer> testMap;

    //ExecutorService executorService = Executors.newFixedThreadPool(15);

    public WordCloudUpdater(String masterIP) {
        parser = new AmazonDescriptionParser();
        tokenizer = new CustomTokenizer(); //CustomTokenizer does not have stemming.
        dataStore = new HazelcastStorageController(masterIP);
        //testMap = new ConcurrentHashMap<>();
    }

    public void handleURL(String URL) throws Exception {
        // For async response
        //executorService.submit(() -> {
        if (dataStore.shouldVisit(URL)) {
            System.out.println("Visting URL: " + URL);
            String content = parser.getContent(URL);
            if (content == null) {
                System.out.println("No content received from: " + URL);
                throw new Exception();
            }
            //System.out.println("Adding content: " + content);
            List<String> tokens = tokenizer.tokenize(content);
            for (String token : tokens) {
                dataStore.updateWordCount(token);
            }
        }
        else {
            System.out.println("Not visiting: " + URL);
        }
      //  });
    }

    public List<Pair> getTopK() {
        /*testMap.entrySet().stream()
                .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                .limit(20)
                .forEach(System.out::println);*/
        return dataStore.getTopK();
    }
}
