package services;

import com.hazelcast.config.*;
import com.hazelcast.core.*;
import com.rethinkdb.RethinkDB;
import com.rethinkdb.net.Connection;
import org.apache.commons.lang3.tuple.Pair;
import persistence.URLStore;
import persistence.WordCountStore;
import persistence.util.URLCounter;
import persistence.util.WordCounter;
import util.ClusterConfig;
import util.Constants;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by amanchalesridhar on 8/5/16.
 */
public class HazelcastStorageController {

    private HazelcastInstance hz;

    private IMap<String, Long> wordCounts;

    private IMap<String, Long> parsedUrls;

    private RethinkDB r;

    private Connection rethinkConn;

    public HazelcastStorageController(String hazelCastNode) {
        Config config = new Config();
        config.getNetworkConfig()
                .getJoin()
                .getTcpIpConfig()
                .addMember(hazelCastNode)
                .setEnabled(true);
        config.getNetworkConfig()
                .getJoin()
                .getMulticastConfig()
                .setEnabled(false);

        createMapStoreConfiguration(config, Constants.URL_SET_NAME, new URLStore(), 0);
        createMapStoreConfiguration(config, Constants.MAP_NAME, new WordCountStore(), Constants.WC_WRITE_DELAY);

        this.hz = Hazelcast.newHazelcastInstance(config);

        URLStore urlStore = new URLStore();
        MapStoreConfig urlStoreConfig = new MapStoreConfig();
        urlStoreConfig.setImplementation(urlStore);
        urlStoreConfig.setWriteDelaySeconds(0);
        this.r = RethinkDB.r;
        this.rethinkConn = r.connection()
                .hostname(ClusterConfig.getDbHostName())
                .port(ClusterConfig.getDbPort())
                .connect();

        createPersistenceTables();
        this.wordCounts = hz.getMap(Constants.MAP_NAME);
        this.parsedUrls = hz.getMap(Constants.URL_SET_NAME);

    }

    private void createPersistenceTables() {
        List tables = r.db("test").tableList().run(rethinkConn);
        if (!tables.contains(Constants.URL_TABLE)) {
            r.tableCreate(Constants.URL_TABLE).run(rethinkConn);
        }
        if (!tables.contains(Constants.WC_TABLE)) {
            r.tableCreate(Constants.WC_TABLE).optArg("primary_key", "word").run(rethinkConn);
        }
    }

    private Config createMapStoreConfiguration(Config config, String mapName, MapStore mapStore, int writeDelay) {
        MapStoreConfig mapStoreConfig = new MapStoreConfig();
        mapStoreConfig.setImplementation(mapStore);
        mapStoreConfig.setWriteDelaySeconds(writeDelay);
        MapConfig mapConfig = config.getMapConfig(mapName);
        mapConfig.setMapStoreConfig(mapStoreConfig);
        return config;
    }

    public boolean shouldVisit(String url) {
        if (parsedUrls.containsKey(url)) {
            return false;
        } else {
            return ((boolean) parsedUrls.executeOnKey(url, new URLCounter()));
        }
    }

    public void updateWordCount(String word) {
        wordCounts.executeOnKey(word, new WordCounter());
    }

    public List<Pair> getTopK() {
        List results = r.table(Constants.WC_TABLE)
                .orderBy(r.desc("count"))
                .limit(Constants.WC_NUM_WORDS)
                .run(rethinkConn);
        List<Pair> top = new ArrayList<>();

        results.stream()
                .forEach(x -> {
                    top.add(Pair.of(((Map) x).get("word"), ((Map) x).get("count")));
                });

        return top;
    }
}
