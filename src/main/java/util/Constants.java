package util;

/**
 * Created by amanchalesridhar on 8/5/16.
 */
public class Constants {

    public static final String MAP_NAME = "wcountmap";

    public static final String URL_SET_NAME = "urlset";

    public static final String URL_TABLE = "urlstore";

    public static final String WC_TABLE = "wcstore";

    public static final int WC_NUM_WORDS = 25;

    public static final int WC_WRITE_DELAY = 5;
}
