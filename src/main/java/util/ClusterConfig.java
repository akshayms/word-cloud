package util;

/**
 * Created by msa on 8/11/16.
 */
public class ClusterConfig {

    private static String dbHostName;

    private static String hazelcastNode;

    private static int dbPort;

    public static String getDbHostName() {
        return dbHostName;
    }

    public static void setDbHostName(String dbHostName) {
        ClusterConfig.dbHostName = dbHostName;
    }

    public static String getHazelcastNode() {
        return hazelcastNode;
    }

    public static void setHazelcastNode(String hazelcastNode) {
        ClusterConfig.hazelcastNode = hazelcastNode;
    }

    public static int getDbPort() {
        return dbPort;
    }

    public static void setDbPort(int dbPort) {
        ClusterConfig.dbPort = dbPort;
    }
}
