package rest;

import org.eclipse.jetty.server.Request;
import services.WordCloudUpdater;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by msa on 8/6/16.
 */
public class AmazonProductRequestHandler extends org.eclipse.jetty.server.handler.AbstractHandler {

    private WordCloudUpdater wordCloudUpdater;

    public AmazonProductRequestHandler(WordCloudUpdater wordCloudUpdater) {
        this.wordCloudUpdater = wordCloudUpdater;
    }

    @Override
    public void handle(String s, Request request, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException, ServletException {
        String URL = request.getParameter("URL");
        System.out.println("New Request: " + URL);
        if (URL != null && !URL.isEmpty()) {
            try {
                wordCloudUpdater.handleURL(URL);
                httpServletResponse.setContentType("text/html");
                httpServletResponse.setStatus(HttpServletResponse.SC_OK);
                httpServletResponse.getWriter().println("OK");
            }
            catch (Exception e) {
                System.out.println("Error occurred while fetching content, this URL will be ignored: " + URL);
                httpServletResponse.getWriter().println("Error parsing this request!");
                httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                e.printStackTrace();
            }
        }
        request.setHandled(true);
    }
}
