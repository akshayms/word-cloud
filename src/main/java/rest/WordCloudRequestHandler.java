package rest;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;
import services.WordCloudUpdater;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by msa on 8/9/16.
 */
public class WordCloudRequestHandler extends AbstractHandler {

    private WordCloudUpdater wordCloudUpdater;

    public WordCloudRequestHandler(WordCloudUpdater wordCloudUpdater) {
        this.wordCloudUpdater = wordCloudUpdater;
    }

    @Override
    public void handle(String s, Request request, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException, ServletException {
        try {
            List<Pair> wordCloud = wordCloudUpdater.getTopK();
            PrintWriter responseWriter = httpServletResponse.getWriter();
            responseWriter.write("<html><body><table><tr><th>Word</th><th>Count</th></tr>");
            wordCloud.stream()
                    .forEach(pair -> {
                        responseWriter.println("<tr>");
                        responseWriter.println("<td>" + pair.getLeft() + "</td>");
                        responseWriter.println("<td>" + pair.getRight() + "</td>");
                        responseWriter.println("</tr>");
                    });
            responseWriter.write("</table></body></html>");
            httpServletResponse.setStatus(HttpServletResponse.SC_OK);
        }
        catch (Exception e) {
            e.printStackTrace();
            httpServletResponse.getWriter().println("Error occurred while calculating topK!");
            httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
        httpServletResponse.setContentType("text/html");
        request.setHandled(true);
    }
}
