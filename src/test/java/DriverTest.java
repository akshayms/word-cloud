import com.hazelcast.core.MapStore;
import org.junit.Test;
import persistence.URLStore;

/**
 * Created by amanchalesridhar on 8/5/16.
 */
public class DriverTest {
    @Test
    public void hello() {
        URLStore u = new URLStore();
        u.store("hello", (long) 1);
        System.out.println(u.load("hello"));
    }
}
